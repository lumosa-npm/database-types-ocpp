# ocpp database types

A project that exposes common code to multiple projects that need to access ocpp data via chargers api https://gitlab.com/charger-microservices/api-chargers

Each type represents the data format expected for a certain collection/table in the database.


### Known projects using these types

https://gitlab.com/charger-microservices/ocpp-web-admin

https://gitlab.com/charger-microservices/lumosa-cpo
