export interface OcppTransactionInfo {
    id: string;
    transactionId: number;
    startDate: string;
    endDate: string;
    meterStart: number;
    meterStop: number;
    tagId: string;
    connectorId: number;
    chargerId: string;
    meterValues: unknown[];
    hubjectSessionId?: string;
    payment?: {
        unit: any;
        startFee: number;
        kwhFee: number;
        idleFee: number;
        parkingFee: number;
        stripePaymentId: string;
    },
    empProvider: string;
}
