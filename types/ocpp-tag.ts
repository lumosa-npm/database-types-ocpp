export interface OcppTagInfo {
  id: string;
  registerOn?: string;
  customerId: string;
  tagId: string;
  userName: string;
  parentId?: string;
}